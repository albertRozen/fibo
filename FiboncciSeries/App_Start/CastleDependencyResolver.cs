﻿using Castle.MicroKernel;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;
using WebApiCastleIntegration.Infrastructure;

namespace FiboncciSeries.App_Start
{
    public class CastleDependencyResolver : System.Web.Http.Dependencies.IDependencyResolver
    {
        private IWindsorContainer _container;
        private bool _disposed;

        public CastleDependencyResolver(IWindsorContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }

            _container = container;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return _container.Resolve(serviceType);
            }
            catch (ComponentNotFoundException)
            {
                return null;
            }
        }

        public IDependencyScope BeginScope()
        {
            return new CastleDependencyScope(_container);
        }


        public IEnumerable<object> GetServices(Type serviceType)
        {
            return _container.ResolveAll(serviceType).Cast<object>();
        }

        public void Dispose()
        {
        	Dispose(true);
			GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    // Cleanup managed resources here, if necessary.
                    if (_container != null)
                    {
                        _container.Dispose();
                        _container = null;
                    }
                }

                // Cleanup any unmanaged resources here, if necessary.
                _disposed = true;
            }

            // Call the base class's Dispose(Boolean) method, if available.
            // base.Dispose( disposing );
        }
    }
}