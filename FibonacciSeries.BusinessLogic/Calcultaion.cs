﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FibonacciSeries.Interfaces;
using FibonacciSeries.Data;

namespace FibonacciSeries.BusinessLogic
{
    public class Calcultaion : IBL
    {
        private DataMemory dt;
        private IDL _dl;

        public Calcultaion(IDL dl)
        {
            _dl = dl;
        }

        public Calcultaion()
        {
            dt = new DataMemory();
            // TODO: Complete member initialization
        }
        public int getNumber(int index)
        {
            int reslt = dt.getNum(index);
           // var reslt = _dl.getNum(index);
            
            if(reslt == 0)
            {
                reslt = calculateFib(index);
                dt.addToData(index, reslt);
                //_dl.addToData(index, reslt);
                return reslt;
            }
            else
            {
                return reslt;
            }
        }

        private int calculateFib(int index)
        {
            int first = 1;
            int second = 1;
            int tmp = 0;

            for (int i = 1; i < index; i++)
            {
                tmp = first;
                first = second;
                second = tmp + second;
            }
            return first;
        }
    }
}
