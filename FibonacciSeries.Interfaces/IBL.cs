﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSeries.Interfaces
{
    public interface IBL
    {
        int getNumber(int index);
    }
}
