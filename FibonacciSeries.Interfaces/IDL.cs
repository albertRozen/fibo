﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciSeries.Interfaces
{
    public interface IDL
    {
        int getNum(int index);

        void addToData(int index, int reslt);
    }
}
