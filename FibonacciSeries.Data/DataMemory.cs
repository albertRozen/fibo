﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FibonacciSeries.Interfaces;
using System.Collections;
using System.Data.SqlClient;

namespace FibonacciSeries.Data
{
    public class DataMemory : IDL
    {
        Hashtable fibSereis = new Hashtable();

        SqlConnection conn = new SqlConnection("Server=.\\SQLEXPRESS;Database=fibData;Integrated Security=true");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader reader;

        public int getNum(int index)
        {
            cmd.Connection = conn;
            conn.Open();
            cmd.CommandText = "SELECT index FROM fibSeries";
            reader = cmd.ExecuteReader();
            if(reader.HasRows)
            {
                while(reader.Read())
                {
                    if (Convert.ToInt32(reader.Read().ToString()) == index)
                    {
                        conn.Close();
                        return Convert.ToInt32(reader.GetString(2));
                    }
                }
            }
            return 0;
            //if (fibSereis.ContainsKey(index) == false)
            //    return 0;

            //return Convert.ToInt32(fibSereis[index].ToString());
        }

        public void addToData(int index, int reslt)
        {
            cmd.Connection = conn;
            conn.Open();
            cmd.CommandText = "insert into fibSeries (index,value) values('"+index+"','"+reslt+"')";
            cmd.ExecuteNonQuery();
            cmd.Clone();
            conn.Close();
            //fibSereis.Add(index, reslt);
        }
    }
}
